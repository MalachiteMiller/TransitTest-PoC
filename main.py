import pandas as pd
import numpy as np
import requests
import zipfile
import io
import matplotlib.pyplot as plt
import mplleaflet
from scipy.spatial.distance import cdist
from datetime import datetime, timedelta
from pytz import timezone
import argparse
import os
plt.switch_backend('agg')


def get_gtfs(now):
    """Download the static GTFS files if they are more than a day old from the RTM, then return them as dataframes"""
    try:
        os.mkdir('gtfs')
    except FileExistsError:
        pass

    try:  # Check when/if the GTFS files where last downloaded
        mtime = datetime.fromtimestamp(os.path.getmtime('gtfs/routes.txt'))
        if timedelta(now.day - mtime.day).days > 0:
            raise FileNotFoundError
    except FileNotFoundError:
        r = requests.get('http://www.rtm.quebec/xdata/trains/google_transit.zip')

        if not r.ok:
            raise ConnectionError('Static GTFS files not found.')

        zipfile.ZipFile(io.BytesIO(r.content)).extractall('gtfs')

    routes = pd.read_csv('gtfs/routes.txt')
    trips = pd.read_csv('gtfs/trips.txt')
    stops = pd.read_csv('gtfs/stops.txt')
    stop_times = pd.read_csv('gtfs/stop_times.txt')
    stop_times['departure_time'] = easy_sort_times(stop_times['departure_time'])
    shapes = pd.read_csv('gtfs/shapes.txt')
    cal = pd.read_csv('gtfs/calendar.txt')
    return routes, trips, stops, stop_times, shapes, cal


def easy_sort_times(column):
    """Convert a column of times in format HH:mm:ss to HHmm"""
    b = np.core.defchararray.partition(column.values.astype('str'), ':')
    c = np.core.defchararray.partition(b[:, 2], ':')
    d = np.core.defchararray.add(b[:, 0], c[:, 0])
    return pd.to_numeric(d)


def find_next_trip(time, route_selection, direction, location, trips, cal, stop_times, stops):
    """Return the next departure from the stop_times dataframe and the closest stop from the stops dataframe"""
    right_rd = trips.loc[(trips['route_id'] == route_selection) & (trips['direction_id'] == direction)]
    ok_service_id = cal.loc[cal[time.strftime('%A').lower()] == 1]['service_id'].values
    right_service = right_rd.loc[right_rd['service_id'].isin(ok_service_id)]
    right_stop_times = stop_times.loc[stop_times['trip_id'].isin(right_service['trip_id'])]
    right_stops = find_trip_stops(right_stop_times, stops)
    closest_right_stop = find_closest_stop(location, right_stops)
    closest_right_stop_departures = right_stop_times.loc[right_stop_times['stop_id'] ==
                                                         closest_right_stop['stop_id'].to_string(index=False)]
    closest_right_stop_departures = closest_right_stop_departures.sort_values(by=['departure_time'])
    now = int(str(time.hour) + str(time.minute))
    try:
        next_trip = closest_right_stop_departures.loc[closest_right_stop_departures['departure_time'] > now].iloc[0]
    except IndexError:
        return "There is no next trip today.", None
    return next_trip, closest_right_stop


def find_trip_shape(trip_id, trips, shapes):
    """Return a copy of the shapes dataframe containing only the shape of the current trip"""
    shape_id = trips.loc[trips['trip_id'] == trip_id]['shape_id'].to_string(index=False)
    shape_latlon = shapes.loc[shapes['shape_id'] == shape_id]
    shape_latlon = shape_latlon.sort_values(by=['shape_pt_sequence'])
    return shape_latlon


def find_trip_stop_times(trip_id, stop_times):
    """Return a copy of the stop_times dataframe containing only the stop_times on the current trip."""
    return stop_times.loc[stop_times['trip_id'] == trip_id]


def find_trip_stops(trip_stop_times, stops):
    """Return a copy of the stops datafrane containing only the stops selected from stop_times"""
    return stops.loc[stops['stop_id'].isin(trip_stop_times['stop_id'])]


def find_closest_stop(location, right_stops):
    """Return the row from the trip_stops dataframe containg the closest stop to the given location."""
    stops_latlon = right_stops[['stop_lat', 'stop_lon']].values
    closest_stop = stops_latlon[cdist(np.array([location]), stops_latlon).argmin()]
    return right_stops.loc[(right_stops['stop_lat'] == closest_stop[0]) & (right_stops['stop_lon'] == closest_stop[1])]


def generate_map(location, route_selection, direction, time):
    """Create _map.html showing the chosen trips shape and nearest stop"""
    routes, trips, stops, stop_times, shapes, cal = get_gtfs(time)

    route, closest_stop = find_next_trip(time, route_selection, direction, location, trips, cal, stop_times, stops)

    try:
        trip_stop_times = find_trip_stop_times(route['trip_id'], stop_times)
    except TypeError:
        print(route)
        exit()
    trip_stops = find_trip_stops(trip_stop_times, stops)

    trip_shape = find_trip_shape(route['trip_id'], trips, shapes)

    plt.plot(location[1], location[0], 'cv')
    plt.plot(trip_stops['stop_lon'], trip_stops['stop_lat'], 'k.')
    plt.plot(closest_stop['stop_lon'], closest_stop['stop_lat'], 'g*')
    plt.plot(trip_shape['shape_pt_lon'].values, trip_shape['shape_pt_lat'].values,
             '#' + routes[routes['route_id'] == route_selection]['route_color'].to_string(index=False))

    mplleaflet.save_html(tiles='cartodb_positron')

    return str(route['departure_time'])[:-2]+':'+str(route['departure_time'])[-2:]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Find the next train.')
    parser.add_argument('lat', metavar='lat', type=float, nargs=1, help='your decimal latitude.')
    parser.add_argument('lon', metavar='lon', type=float, nargs=1, help='your decimal longitude.')
    parser.add_argument('route', metavar='route', type=int, nargs=1, help='the number of the route you want to take.')
    parser.add_argument('dir', metavar='direction', type=int, nargs=1, help='the direction you want to travel in. 0 '
                                                                            'is in to Montreal, 1 is out.')
    args = parser.parse_args()

    location = [args.lat[0], args.lon[0]]
    route_selection = args.route[0]
    direction = args.dir[0]

    time = datetime.now(timezone('US/Eastern'))
    print('Next departure: ' + generate_map(location, route_selection, direction, time))
